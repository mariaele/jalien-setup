# Fix systemd-resolved problem
apt-get update;
apt-get install -y ca-certificates curl gnupg lsb-release

# setup MySQL
apt-get install -y debconf-utils;
{ \
echo mysql-community-server mysql-community-server/root-pass password ''; \
echo mysql-community-server mysql-community-server/re-root-pass password ''; \
} | debconf-set-selections \
&& apt-get install -y mysql-server

# Install dependencies
export DEBIAN_FRONTEND=noninteractive
export LC_ALL=C
apt-get install -y openjdk-11-jdk python3 python3-pip git slapd ldap-utils rsync vim tmux entr less cmake zlib1g-dev uuid uuid-dev libssl-dev

# Install XRootD
apt install -y wget pkg-config
wget https://xrootd.slac.stanford.edu/download/v5.6.0/xrootd-5.6.0.tar.gz
tar xvzf xrootd-5.6.0.tar.gz
mkdir /build && cd /build
cmake /xrootd-5.6.0 -DCMAKE_INSTALL_PREFIX=/usr -DENABLE_PERL=FALSE
make && make install
cd /

#Install HTCondor
apt-get update && apt-get install -y curl
central_manager_name="schedd" 
curl -fsSL https://get.htcondor.org | GET_HTCONDOR_PASSWORD="$htcondor_password" /bin/bash -s -- --no-dry-run --submit $central_manager_name
apt-get install -y supervisor environment-modules tcl

