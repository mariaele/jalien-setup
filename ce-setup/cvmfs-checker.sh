#!/bin/bash

# All packages that may be needed for the worker
# Source: https://gitlab.cern.ch/jalien/jalien-jobcontainer/-/blob/el8/alice_el8.def
function install_all_dependencies() {
	rpmdb --rebuilddb && dnf clean all && rm -rf /var/cache/dnf
	dnf -y install dnf-plugins-core
	dnf config-manager --set-enabled powertools
	dnf update -y

	dnf install -y attr autoconf automake avahi-compat-libdns_sd-devel bc bind-export-libs bind-libs bind-libs-lite bind-utils binutils bison bzip2-devel cmake compat-libgfortran-48 e2fsprogs e2fsprogs-libs environment-modules fftw-devel file-devel flex gcc gcc-c++ gcc-gfortran git glew-devel glibc-devel glibc-static gmp-devel graphviz-devel java-17-openjdk libcurl-devel libpng-devel libtool libX11-devel libXext-devel libXft-devel libxml2-devel libXmu libXpm-devel libyaml-devel mesa-libGL-devel mesa-libGLU-devel motif-devel mpfr-devel mysql-devel ncurses-devel numactl-devel numactl-libs openldap-devel openssl-devel pciutils-devel pcre-devel perl-ExtUtils-Embed perl-libwww-perl protobuf-devel python3-devel readline-devel redhat-lsb rpm-build swig tcl tcsh texinfo tk-devel unzip uuid-devel wget which zip zlib-devel zlib-static zsh strace net-tools xz
	mkdir /cvmfs
	mkdir /workdir
	dnf clean all
	dnf update -y

}

yum install -y environment-modules tcl libX11

# install_all_dependencies

if [[ -d /cvmfs/alice.cern.ch/bin ]] 
then {
    bash /start.sh
    exit
}
else {
    #add appropriate symlinks to allow functioning of agent startup script

    #start htcondor
    bash /start.sh
}
fi
